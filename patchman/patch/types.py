from typing import List

from attrs import field, frozen

from patchman.types import Pos


@frozen
class Patch:
    offsets: List[Pos] = field(kw_only=True)
    button_cost: int = field(kw_only=True)
    time_cost: int = field(kw_only=True)
    income: int = field(kw_only=True, default=0)

    def __str__(self) -> str:
        min_r = min(p.r for p in self.offsets)
        min_c = min(p.c for p in self.offsets)
        offset_strs = [f"{pos.r - min_r}{pos.c - min_c}" for pos in self.offsets]
        return f"{''.join(sorted(offset_strs))};{self.button_cost};{self.time_cost};{self.income}"

    @classmethod
    def from_string(cls, string: str) -> "Patch":
        offset_strs, button_cost, time_cost, income = tuple(string.split(";"))
        offsets = [
            Pos(int(offset_strs[i * 2]), int(offset_strs[i * 2 + 1]))
            for i in range(len(offset_strs) // 2)
        ]
        assert min(p.r for p in offsets) == 0
        assert min(p.c for p in offsets) == 0

        return cls(
            offsets=offsets,
            button_cost=int(button_cost),
            time_cost=int(time_cost),
            income=int(income),
        )


@frozen
class PatchPlacement:
    offsets: List[Pos] = field(kw_only=True)
    income: int = field(kw_only=True, default=0)

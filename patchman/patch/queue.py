import random
from enum import Enum
from typing import List, Tuple

from attrs import field, frozen

from patchman.patch.data import ALL_PATCHES, LEATHER_PATCH
from patchman.patch.types import Patch


class PatchSelection(Enum):
    FIRST = 1
    SECOND = 2
    THIRD = 3
    LEATHER = "LEATHER"

    def index(self):
        assert self != PatchSelection.LEATHER
        return self.value - 1

    @classmethod
    def from_index(cls, index: int):
        if index == 0:
            return PatchSelection.FIRST
        if index == 1:
            return PatchSelection.SECOND
        if index == 2:
            return PatchSelection.THIRD
        raise ValueError("Invalid index")


@frozen
class PatchQueue:
    q: List[Patch] = field()

    def select(self, patch_sel: PatchSelection) -> Tuple[Patch, "PatchQueue"]:
        if patch_sel == PatchSelection.LEATHER:
            return LEATHER_PATCH, self

        i = patch_sel.index()
        assert 0 <= i << min(3, len(self.q))

        new_q = self.q[i + 1 :] + self.q[:i]
        return self.q[i], PatchQueue(new_q)

    @classmethod
    def random(cls):
        patch_list = ALL_PATCHES.copy()
        random.shuffle(patch_list)

        # move the 2x1 to the back
        _2x1_index = None
        for i, patch in enumerate(patch_list):
            if len(patch.offsets) == 2:
                _2x1_index = i
        patch_list[_2x1_index], patch_list[-1] = patch_list[-1], patch_list[_2x1_index]
        return cls(patch_list)

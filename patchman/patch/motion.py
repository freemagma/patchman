from enum import Enum
from typing import List

from attrs import evolve, field, frozen

from patchman.patch.types import Patch, PatchPlacement
from patchman.types import Pos


class Affine(Enum):
    ROTATE_CW = 1
    FLIP_Y = 2


def _simplify(affines: List[Affine]) -> List[Affine]:
    rotations = 0
    flipped = False
    for affine in reversed(affines):
        if affine == Affine.ROTATE_CW:
            rotations += -1 if flipped else 1
        else:
            flipped = not flipped

    simplified = []
    if flipped:
        simplified.append(Affine.FLIP_Y)
    simplified.extend([Affine.ROTATE_CW] * (rotations % 4))

    return simplified


@frozen
class Motion:
    affines: List[Affine] = field(kw_only=True, factory=list)
    translation: Pos = field(kw_only=True, default=Pos(0, 0))

    def rotate_cw(self) -> "Motion":
        new_syms = self.affines + [Affine.ROTATE_CW]
        return evolve(self, affines=_simplify(new_syms))

    def rotate_ccw(self) -> "Motion":
        new_syms = self.affines + [Affine.ROTATE_CW] * 3
        return evolve(self, affines=_simplify(new_syms))

    def flip_y(self) -> "Motion":
        new_syms = self.affines + [Affine.FLIP_Y]
        return evolve(self, affines=_simplify(new_syms))

    def flip_x(self) -> "Motion":
        new_syms = self.affines + [Affine.FLIP_Y] + [Affine.ROTATE_CW] * 2
        return evolve(self, affines=_simplify(new_syms))

    def translate(self, pos: Pos) -> "Motion":
        return evolve(self, translation=self.translation + pos)

    def act_on(self, patch: Patch) -> PatchPlacement:
        offsets = patch.offsets
        for affine in self.affines:
            if affine == Affine.FLIP_Y:
                offsets = [Pos(p.r, -p.c) for p in offsets]
            else:
                offsets = [Pos(p.c, -p.r) for p in offsets]

        min_r = min(p.r for p in offsets)
        min_c = min(p.c for p in offsets)
        offsets = [
            Pos(p.r - min_r + self.translation.r, p.c - min_c + self.translation.c)
            for p in offsets
        ]
        return PatchPlacement(offsets=offsets, income=patch.income)

from enum import Enum
from typing import Optional

from attrs import evolve, field, frozen
from getkey import keys

from patchman.move import AdvanceMove, Move, PlacePatchMove
from patchman.patch import Motion, PatchPlacement, PatchSelection
from patchman.state import GameState
from patchman.types import Pos


class InteractiveMoveState(Enum):
    PATCH_SELECTION = 1
    ADVANCE_HOVER = 2
    MOTION = 3
    DONE = 4


@frozen
class InteractiveMove:
    state: GameState = field(kw_only=True)
    istate: InteractiveMoveState = field(kw_only=True)
    move: Move = field(kw_only=True)
    past_patch_place_move: Optional[PlacePatchMove] = field(kw_only=True, default=None)

    def update(self, key) -> "InteractiveMove":
        imove = self

        match self.move:
            case AdvanceMove():
                if self.istate == InteractiveMoveState.ADVANCE_HOVER:
                    if key == keys.ENTER:
                        imove = evolve(self, istate=InteractiveMoveState.DONE)
                    elif key in (keys.UP, "w"):
                        imove = evolve(
                            self,
                            move=self.past_patch_place_move,
                            istate=InteractiveMoveState.PATCH_SELECTION
                            or default_place_patch_move(self.state),
                        )

            case PlacePatchMove(patch_sel=patch_sel, motion=motion):
                if self.istate == InteractiveMoveState.PATCH_SELECTION:
                    if key == keys.ENTER and self.affordable_selection():
                        imove = evolve(self, istate=InteractiveMoveState.MOTION)
                    elif key in (keys.LEFT, "a"):
                        new_index = max(0, patch_sel.index() - 1)
                        imove = self.evolve_move(
                            patch_sel=PatchSelection.from_index(new_index)
                        )
                    elif key in (keys.RIGHT, "d"):
                        new_index = min(2, patch_sel.index() + 1)
                        imove = self.evolve_move(
                            patch_sel=PatchSelection.from_index(new_index)
                        )
                    elif key in (keys.DOWN, "s"):
                        imove = evolve(
                            self,
                            move=AdvanceMove(),
                            istate=InteractiveMoveState.ADVANCE_HOVER,
                            past_patch_place_move=self.move,
                        )

                elif self.istate == InteractiveMoveState.MOTION:
                    if key == keys.ENTER and self:
                        player = self.state.next_turn()
                        if self.state.quilt_board[player].fits(self.patch_placement()):
                            imove = evolve(self, istate=InteractiveMoveState.DONE)
                    elif key == keys.ESC and not patch_sel == PatchSelection.LEATHER:
                        imove = evolve(
                            self, istate=InteractiveMoveState.PATCH_SELECTION
                        )
                    elif key == "f":
                        imove = self.evolve_move(motion=motion.flip_y())
                    elif key == "q":
                        imove = self.evolve_move(motion=motion.rotate_ccw())
                    elif key == "e":
                        imove = self.evolve_move(motion=motion.rotate_cw())
                    elif key in (keys.LEFT, "a"):
                        imove = self.evolve_move(motion=motion.translate(Pos(0, -1)))
                    elif key in (keys.RIGHT, "d"):
                        imove = self.evolve_move(motion=motion.translate(Pos(0, 1)))
                    elif key in (keys.UP, "w"):
                        imove = self.evolve_move(motion=motion.translate(Pos(-1, 0)))
                    elif key in (keys.DOWN, "s"):
                        imove = self.evolve_move(motion=motion.translate(Pos(1, 0)))

        return imove.clamp()

    def affordable_selection(self) -> bool:
        match self.move:
            case AdvanceMove():
                return True
            case PlacePatchMove(patch_sel=patch_sel):
                patch, _ = self.state.patch_queue.select(patch_sel)
                return self.state.buttons[self.state.next_turn()] >= patch.button_cost

    def patch_placement(self) -> Optional[PatchPlacement]:
        if self.istate != InteractiveMoveState.MOTION:
            return None
        match self.move:
            case AdvanceMove():
                return None
            case PlacePatchMove(patch_sel=patch_sel, motion=motion):
                patch, _ = self.state.patch_queue.select(patch_sel)
                return motion.act_on(patch)

    def clamp(self) -> "InteractiveMove":
        match self.move:
            case AdvanceMove():
                return self
            case PlacePatchMove(patch_sel=patch_sel, motion=motion):
                patch, _ = self.state.patch_queue.select(patch_sel)
                patch_placement = motion.act_on(patch)

                fixup_r, fixup_c = 0, 0
                for p in patch_placement.offsets:
                    if p.r < 0:
                        fixup_r = max(-p.r, fixup_r)
                    if p.r >= 9:
                        fixup_r = min(8 - p.r, fixup_r)
                    if p.c < 0:
                        fixup_c = max(-p.c, fixup_c)
                    if p.c >= 9:
                        fixup_c = min(8 - p.c, fixup_c)

                return self.evolve_move(motion=motion.translate(Pos(fixup_r, fixup_c)))

    def evolve_move(self, **kwargs) -> "InteractiveMove":
        return evolve(self, move=evolve(self.move, **kwargs))


def default_place_patch_move(state: GameState) -> PlacePatchMove:
    if state.await_leather_move:
        return PlacePatchMove(patch_sel=PatchSelection.LEATHER, motion=Motion())
    return PlacePatchMove(patch_sel=PatchSelection.FIRST, motion=Motion())


def default_imove(state: GameState) -> InteractiveMove:
    return InteractiveMove(
        state=state,
        istate=InteractiveMoveState.MOTION
        if state.await_leather_move
        else InteractiveMoveState.PATCH_SELECTION,
        move=default_place_patch_move(state),
    )

import itertools as it
from typing import FrozenSet

from attrs import evolve, field, frozen

from patchman.patch import PatchPlacement
from patchman.types import Pos


@frozen
class QuiltBoard:
    covered_positions: FrozenSet[Pos] = field(factory=frozenset)
    income: int = field(default=0)

    def fits(self, placement: PatchPlacement) -> bool:
        for pos in self.covered_positions:
            if pos in placement.offsets:
                return False
        return True

    def place(self, placement: PatchPlacement) -> "QuiltBoard":
        assert self.fits(placement)
        return evolve(
            self,
            covered_positions=self.covered_positions | frozenset(placement.offsets),
            income=self.income + placement.income,
        )

    def has_7x7(self) -> bool:
        ranges = [(x, x + 7) for x in range(3)]
        for row_range, col_range in it.product(ranges, repeat=2):
            if all(
                Pos(r, c) in self.covered_positions
                for r, c in it.product(range(*row_range), range(*col_range))
            ):
                return True
        return False

from attrs import field, frozen

from patchman.patch import Motion, PatchSelection


@frozen
class AdvanceMove:
    pass


@frozen
class PlacePatchMove:
    patch_sel: PatchSelection = field(kw_only=True)
    motion: Motion = field(kw_only=True)


Move = AdvanceMove | PlacePatchMove

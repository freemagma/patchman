from enum import Enum
from typing import Optional, Tuple, Union

from attrs import field, frozen

from patchman.move import AdvanceMove, Move, PlacePatchMove
from patchman.patch import PatchQueue, PatchSelection
from patchman.quilt_board import QuiltBoard
from patchman.time_board import TimeBoard, TimeBoardMoveResult, TimeBoardSpace
from patchman.types import Player, PlayerPair


class MakeMoveError(Enum):
    PATCH_OFF_BOARD = 1
    PATCH_COLLIDES = 2
    MUST_PLACE_LEATHER = 3
    CANNOT_PLACE_LEATHER = 4
    TOO_EXPENSIVE = 5


@frozen
class GameState:
    quilt_board: PlayerPair[QuiltBoard] = field(
        kw_only=True, default=PlayerPair(QuiltBoard(), QuiltBoard())
    )
    time_board: TimeBoard = field(kw_only=True)
    buttons: PlayerPair[int] = field(kw_only=True, default=PlayerPair(5, 5))
    patch_queue: PatchQueue = field(kw_only=True)
    await_leather_move: Optional[Player] = field(kw_only=True, default=None)
    sevens_winner: Optional[Player] = field(kw_only=True, default=None)
    first_finisher: Optional[Player] = field(kw_only=True, default=None)

    def next_turn(self) -> Player:
        if self.await_leather_move is not None:
            return self.await_leather_move
        return self.time_board.furthest_behind()

    def make_move(self, move: Move) -> Union["GameState", MakeMoveError]:
        player = self.next_turn()
        quilt_board = self.quilt_board
        time_board = self.time_board
        buttons = self.buttons
        patch_queue = self.patch_queue

        move_result = TimeBoardMoveResult()
        match move:
            case PlacePatchMove(patch_sel=patch_sel, motion=motion):
                if patch_sel == PatchSelection.LEATHER and not self.await_leather_move:
                    return MakeMoveError.CANNOT_PLACE_LEATHER
                if patch_sel != PatchSelection.LEATHER and self.await_leather_move:
                    return MakeMoveError.MUST_PLACE_LEATHER

                patch, patch_queue = patch_queue.select(patch_sel)
                move_result, time_board = time_board.advance(player, patch.time_cost)

                buttons = buttons.update(player, lambda b: b - patch.button_cost)
                if buttons[player] < 0:
                    return MakeMoveError.TOO_EXPENSIVE

                patch_placement = motion.act_on(patch)
                for p in patch_placement.offsets:
                    if p.r < 0 or p.r >= 9 or p.c < 0 or p.c >= 9:
                        return MakeMoveError.PATCH_OFF_BOARD
                if not quilt_board[player].fits(patch_placement):
                    return MakeMoveError.PATCH_COLLIDES

                quilt_board = quilt_board.update(
                    player, lambda qb: qb.place(motion.act_on(patch))
                )

            case AdvanceMove():
                if self.await_leather_move:
                    return MakeMoveError.MUST_PLACE_LEATHER

                move_result, time_board = time_board.advance(player)
                buttons = buttons.update(player, lambda b: b + move_result.spaces_moved)

        await_leather_move = None
        for action in move_result.nonempty_actions:
            if action == TimeBoardSpace.INCOME:
                buttons = buttons.update(
                    player, lambda b: b + quilt_board[player].income
                )
            elif action == TimeBoardSpace.LEATHER_PATCH:
                await_leather_move = player

        return GameState(
            quilt_board=quilt_board,
            time_board=time_board,
            buttons=buttons,
            patch_queue=patch_queue,
            await_leather_move=await_leather_move,
            sevens_winner=self.sevens_winner
            or (player if quilt_board[player].has_7x7() else None),
            first_finisher=self.first_finisher
            or (player if time_board.finished(player) else None),
        )

    def winner(self) -> Optional[Tuple[Player, PlayerPair[int]]]:
        if not all(self.time_board.finished(p) for p in Player):
            return None

        points = PlayerPair[int].make(
            lambda p: self.buttons[p]
            - 2 * (81 - len(self.quilt_board[p].covered_positions))
            + (7 if p == self.sevens_winner else 0)
        )
        winner = max(
            Player, key=lambda p: (points[p], 1 if self.first_finisher == p else 0)
        )

        return winner, points

    @classmethod
    def new_game(cls, first_player: Player):
        patch_queue = PatchQueue.random()
        return cls(
            patch_queue=patch_queue, time_board=TimeBoard(last_mover=first_player)
        )

from typing import Dict, List, Optional, Tuple

from attrs import field, frozen
from rich.console import Console, Group
from rich.panel import Panel
from rich.text import Text

from patchman.interactive_move import InteractiveMove
from patchman.move import AdvanceMove, PlacePatchMove
from patchman.patch import Patch, PatchPlacement, PatchQueue, PatchSelection
from patchman.quilt_board import QuiltBoard
from patchman.state import GameState
from patchman.time_board import TIME_BOARD_SPACES, TimeBoard, TimeBoardSpace
from patchman.types import Player, PlayerPair


def boxify(text: Text, style="") -> Text:
    lines = text.split()
    width = max(l.cell_len for l in lines)
    output_lines = []
    output_lines.append(Text("╭" + "─" * width + "╮", style))
    for line in lines:
        text = Text("│", style)
        text.append_text(line)
        text.append("│", style)
        output_lines.append(text)
    output_lines.append(Text("╰" + "─" * width + "╯", style))

    return Text("\n").join(output_lines)


def join_text_horiz(
    console: Console,
    texts: List[Text],
    hsep: str = "   ",
    vsep: str = "\n\n",
    leave_width: int = 0,
) -> Text:

    by_lines = [t.split() for t in texts]
    widths = [max(l.cell_len for l in lines) for lines in by_lines]
    for width, lines in zip(widths, by_lines):
        for line in lines:
            line.pad_right(width - line.cell_len)

    output_rows = []
    cur_row_ixs: List[int] = []

    def complete_row():
        output_row_lines = []
        height = max(len(by_lines[i]) for i in cur_row_ixs)
        for j in range(height):
            pieces = [
                by_lines[i][j] if j < len(by_lines[i]) else Text(" " * widths[i])
                for i in cur_row_ixs
            ]
            output_row_lines.append(Text(hsep).join(pieces))
        output_rows.append(Text("\n").join(output_row_lines))

    i = 0
    while i < len(texts):
        cur_row_ixs.append(i)
        if (
            sum(widths[i] for i in cur_row_ixs) + len(hsep) * (len(cur_row_ixs) - 1)
            > console.size.width - leave_width
        ):
            cur_row_ixs.pop()
            if len(cur_row_ixs) == 0:
                raise ValueError("Console is not wide enough")
            complete_row()
            cur_row_ixs = []

        else:
            i += 1
    complete_row()

    return Text(vsep).join(output_rows)


BUTTON_CHAR = "⍟"
TIME_CHAR = "t"
SQUARE_CHARS = "[]"
PLAYER_STYLE_DICT = {
    None: "white",
    Player.GREEN: "green",
    Player.YELLOW: "yellow",
}

Annotation = Tuple[
    int, Optional[str | Tuple[str, str]], Optional[str | Tuple[str, str]]
]


@frozen
class PrettyFormatter:
    console: Console = field()

    def format_patch(self, patch: Patch, style=Optional[str]) -> Text:
        min_r = min(pos.r for pos in patch.offsets)
        max_r = max(pos.r for pos in patch.offsets)
        min_c = min(pos.c for pos in patch.offsets)
        max_c = max(pos.c for pos in patch.offsets)

        height = max(3, 1 + max_r - min_r)
        width = 1 + max_c - min_c

        chars = [["  " for _ in range(width)] for _ in range(height)]
        for pos in patch.offsets:
            chars[pos.r - min_r][pos.c - min_c] = SQUARE_CHARS

        lines = [Text("".join(c for c in row), style=style or "white") for row in chars]
        lines[0].append(f" -{patch.button_cost} {BUTTON_CHAR}", style="red")
        lines[1].append(f" -{patch.time_cost} {TIME_CHAR}", style="red")
        lines[2].append(f" +{patch.income} {BUTTON_CHAR}", style="blue")

        return Text("\n").join(lines)

    def format_patch_queue(
        self,
        patch_queue: PatchQueue,
        highlight: Optional[Tuple[PatchSelection, str]] = None,
    ) -> Panel:

        patch_sel, style = (None, None) if highlight is None else highlight
        formatted_patches = [
            self.format_patch(
                patch,
                style=style
                if patch_sel is not None
                and patch_sel != PatchSelection.LEATHER
                and patch_sel.index() == i
                else None,
            )
            for i, patch in enumerate(patch_queue.q)
        ]
        return Panel(
            join_text_horiz(self.console, formatted_patches, leave_width=6),
            expand=False,
        )

    def format_time_board(
        self, time_board: TimeBoard, annotate_tb: Optional[Annotation] = None
    ) -> Panel:

        player_to_targs: Dict[Optional[Player], str | Tuple[str, str]] = {
            None: " ",
            Player.GREEN: ("g", "bold green"),
            Player.YELLOW: ("y", "bold yellow"),
        }
        tb_space_to_targs: Dict[TimeBoardSpace, str | Tuple[str, str]] = {
            TimeBoardSpace.EMPTY: "  ",
            TimeBoardSpace.LEATHER_PATCH: SQUARE_CHARS,
            TimeBoardSpace.INCOME: (f"+{BUTTON_CHAR}", "blue"),
        }

        space_texts = []
        for i, space in enumerate(TIME_BOARD_SPACES):
            left, right = None, None
            for p in (time_board.last_mover, time_board.last_mover.opposite()):
                if time_board.index[p] == i:
                    left, right = right, p

            left, right = tuple(player_to_targs[p] for p in (left, right))
            if annotate_tb is not None and annotate_tb[0] == i:
                left = annotate_tb[1] or left
                right = annotate_tb[2] or right

            space_texts.append(
                Text.assemble(left, right, "\n──\n", tb_space_to_targs[space])
            )

        return Panel(
            join_text_horiz(self.console, space_texts, leave_width=6, hsep=" "),
            expand=False,
        )

    def format_quilt_board(
        self,
        quilt_board: QuiltBoard,
        player: Optional[Player] = None,
        hover_placement: Optional[PatchPlacement] = None,
    ) -> Text:

        chars: List[List[str | Tuple[str, str]]] = [
            ["  " for _ in range(9)] for _ in range(9)
        ]
        for pos in quilt_board.covered_positions:
            chars[pos.r][pos.c] = (SQUARE_CHARS, "bold " + PLAYER_STYLE_DICT[player])

        if hover_placement:
            style = "bold white" if quilt_board.fits(hover_placement) else "bold red"
            for pos in hover_placement.offsets:
                chars[pos.r][pos.c] = (SQUARE_CHARS, style)

        return boxify(
            Text("\n").join(Text.assemble(*row) for row in chars),
            style=PLAYER_STYLE_DICT[player],
        )

    def format_imove(self, imove: InteractiveMove) -> Group:
        player = imove.state.next_turn()
        style = f"bold {PLAYER_STYLE_DICT[player]}"
        match imove.move:
            case AdvanceMove():
                move_result, _ = imove.state.time_board.advance(player)
                index = imove.state.time_board.index[player] + move_result.spaces_moved
                return self.format_game_state(
                    imove.state,
                    annotate_tb=(
                        index,
                        ("+", style),
                        (str(move_result.spaces_moved), style),
                    ),
                )
            case PlacePatchMove(patch_sel=patch_sel):
                patch, _ = imove.state.patch_queue.select(patch_sel)
                move_result, _ = imove.state.time_board.advance(player, patch.time_cost)
                index = imove.state.time_board.index[player] + move_result.spaces_moved
                return self.format_game_state(
                    imove.state,
                    highlight=(
                        patch_sel,
                        f"bold {PLAYER_STYLE_DICT[imove.state.next_turn()]}"
                        if imove.affordable_selection()
                        else "bold red",
                    ),
                    hover_placement=imove.patch_placement(),
                    annotate_tb=(
                        index,
                        ("▶", style),
                        None,
                    )
                    if imove.affordable_selection()
                    else None,
                )

    def format_game_state(
        self,
        state: GameState,
        annotate_tb: Optional[Annotation] = None,
        highlight: Optional[Tuple[PatchSelection, str]] = None,
        hover_placement: Optional[PatchPlacement] = None,
    ) -> Group:

        pq_panel = self.format_patch_queue(state.patch_queue, highlight=highlight)
        tb_panel = self.format_time_board(state.time_board, annotate_tb=annotate_tb)

        qb_texts = [
            self.format_quilt_board(
                state.quilt_board[p],
                player=p,
                hover_placement=hover_placement if p == state.next_turn() else None,
            )
            for p in Player
        ]
        for p, text in zip(Player, qb_texts):
            text.append(
                f"\n {state.buttons[p]} {BUTTON_CHAR}", f"bold {PLAYER_STYLE_DICT[p]}"
            )
            text.append(f" (+{state.quilt_board[p].income} {BUTTON_CHAR})", "bold blue")
            if p == state.sevens_winner:
                text.append(" 7x7", "bold white")

        qb_text = join_text_horiz(self.console, qb_texts, hsep=" ")
        return Group(pq_panel, tb_panel, qb_text)

    def format_winner(self, winner: Player, points: PlayerPair[int]) -> Text:
        name = PlayerPair[str]("Green:  ", "Yellow: ")
        extra = PlayerPair[str].make(lambda p: (" ✔" if winner == p else ""))
        style = PlayerPair[str].make(
            lambda p: ("bold " if winner == p else "") + PLAYER_STYLE_DICT[p]
        )

        pieces = [("Final Score\n", "bold underline white")] + [
            (f"{name[p]}{points[p]}{extra[p]}\n", style[p]) for p in Player
        ]
        return Text.assemble(*pieces)

from enum import Enum
from typing import Callable, Generic, TypeVar

from attrs import field, frozen


@frozen
class Pos:
    r: int = field()
    c: int = field()

    def __add__(self, other) -> "Pos":
        return Pos(self.r + other.r, self.c + other.c)


@frozen
class Human:
    pass


@frozen
class Engine:
    command: str = field()


PlayerType = Human | Engine


class Player(Enum):
    GREEN = 1
    YELLOW = 2

    def opposite(self):
        return Player.YELLOW if self == Player.GREEN else Player.GREEN

    @classmethod
    def from_string(cls, string):
        if string.lower() == "green":
            return cls.GREEN
        if string.lower() == "yellow":
            return cls.YELLOW
        raise ValueError(
            f"{string} is an invalid player string. Please use `green` or `yellow`"
        )


T = TypeVar("T")


@frozen
class PlayerPair(Generic[T]):
    green_val: T = field()
    yellow_val: T = field()

    def __getitem__(self, player: Player):
        return self.green_val if player == Player.GREEN else self.yellow_val

    def update(self, player: Player, f: Callable[[T], T]) -> "PlayerPair[T]":
        new_green = f(self.green_val) if player == Player.GREEN else self.green_val
        new_yellow = f(self.yellow_val) if player == Player.YELLOW else self.yellow_val
        return PlayerPair[T](new_green, new_yellow)

    @classmethod
    def make(cls, f: Callable[[Player], T]) -> "PlayerPair[T]":
        return cls(f(Player.GREEN), f(Player.YELLOW))


@frozen
class RunParams:
    player_type: PlayerPair[PlayerType] = field(kw_only=True)
    first_player: Player = field(kw_only=True)

import argparse
import random

from patchman.types import Engine, Human, Player, PlayerPair, PlayerType, RunParams


def get_params() -> RunParams:
    parser = argparse.ArgumentParser(description="A game arbiter for Patchwork")

    parser.add_argument(
        "-g", "--engine-green", help="Command to start the engine playing green"
    )
    parser.add_argument(
        "-y", "--engine-yellow", help="Command to start the engine playing yellow"
    )
    parser.add_argument(
        "-f", "--first-player", choices=["green", "yellow", "random"], default="random"
    )

    args = parser.parse_args()

    player_type = PlayerPair[PlayerType](
        Human() if not args.engine_green else Engine(args.engine_green),
        Human() if not args.engine_yellow else Engine(args.engine_yellow),
    )
    first_player = Player.from_string(
        args.first_player
        if args.first_player != "random"
        else random.choice(["green", "yellow"])
    )

    return RunParams(player_type=player_type, first_player=first_player)

import time

from attrs import evolve
from getkey import getkey
from rich.console import Console
from rich.live import Live

from patchman.formatter import PrettyFormatter
from patchman.interactive_move import InteractiveMoveState, default_imove
from patchman.move import PlacePatchMove
from patchman.parse_args import get_params
from patchman.patch import ALL_PATCHES, Motion, PatchSelection
from patchman.state import GameState, MakeMoveError
from patchman.types import Human


def run_patchman():
    console = Console()
    pf = PrettyFormatter(console)

    params = get_params()
    state = GameState.new_game(first_player=params.first_player)

    with Live(pf.format_game_state(state), refresh_per_second=25) as live:
        while state.winner() is None:
            player = state.next_turn()
            assert isinstance(
                params.player_type[player], Human
            ), "Engines not supported yet"

            imove = default_imove(state)
            live.update(pf.format_imove(imove))
            while imove.istate != InteractiveMoveState.DONE:
                key = getkey()
                imove = imove.update(key)
                live.update(pf.format_imove(imove))

            result = state.make_move(imove.move)
            if isinstance(result, GameState):
                state = result

        live.update(pf.format_game_state(state))

    console.print()
    console.print(pf.format_winner(*state.winner()))

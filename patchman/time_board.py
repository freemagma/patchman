from enum import Enum
from typing import List, Tuple

from attrs import evolve, field, frozen

from patchman.types import Player, PlayerPair


class TimeBoardSpace(Enum):
    EMPTY = 1
    INCOME = 2
    LEATHER_PATCH = 3


TIME_BOARD_SPACES = ([TimeBoardSpace.EMPTY] * 5 + [TimeBoardSpace.INCOME]) * 4 + (
    [TimeBoardSpace.EMPTY] * 2
    + [TimeBoardSpace.LEATHER_PATCH]
    + [TimeBoardSpace.EMPTY] * 2
    + [TimeBoardSpace.INCOME]
) * 5


@frozen
class TimeBoardMoveResult:
    spaces_moved: int = field(kw_only=True, default=0)
    nonempty_actions: List[TimeBoardSpace] = field(kw_only=True, factory=list)


@frozen
class TimeBoard:
    last_mover: Player = field(kw_only=True)
    index: PlayerPair[int] = field(kw_only=True, default=PlayerPair(0, 0))

    def furthest_behind(self) -> Player:
        g, y = self.index[Player.GREEN], self.index[Player.YELLOW]
        if g < y:
            return Player.GREEN
        if y < g:
            return Player.YELLOW
        return self.last_mover

    def advance(
        self, player: Player, spaces: int = None
    ) -> Tuple[TimeBoardMoveResult, "TimeBoard"]:

        player_index = self.index[player]
        opposite_index = self.index[player.opposite()]

        if spaces is None:
            spaces = opposite_index - player_index + 1

        spaces_moved = min(spaces, len(TIME_BOARD_SPACES) - player_index - 1)
        nonempty_actions = []
        for i in range(player_index + 1, player_index + spaces_moved + 1):
            space = TIME_BOARD_SPACES[i]
            if space == TimeBoardSpace.INCOME or (
                space == TimeBoardSpace.LEATHER_PATCH and i > opposite_index
            ):
                nonempty_actions.append(space)

        new_self = evolve(
            self,
            index=self.index.update(player, lambda i: i + spaces_moved),
            last_mover=player,
        )
        result = TimeBoardMoveResult(
            spaces_moved=spaces_moved, nonempty_actions=nonempty_actions
        )

        return result, new_self

    def finished(self, player: Player) -> bool:
        return self.index[player] == len(TIME_BOARD_SPACES) - 1

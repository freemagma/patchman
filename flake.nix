{
  description = "my project description";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        python = pkgs.python3;
        overrides = pkgs.poetry2nix.overrides.withDefaults (self: super: {
          getkey = python.pkgs.getkey;
        });
      in
      {
        legacyPackages.patchman = pkgs.poetry2nix.mkPoetryApplication {
          inherit python overrides;
          projectDir = ./.;
        };
        packages = flake-utils.lib.flattenTree self.legacyPackages.${system};
        devShell = let poetryEnv = pkgs.poetry2nix.mkPoetryEnv
          {
            inherit python overrides;
            projectDir = ./.;
            editablePackageSources = { patchman = "./patchman"; };
          }; in
          poetryEnv.env.overrideAttrs (old: {
            buildInputs = [ pkgs.poetry pkgs.mypy ];
          });
      });
}
